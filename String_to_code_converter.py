from numpy.core.defchararray import lower, isupper


class StrCode:
    alphabetWages = {"a": 1, "b": 2, "c": 3, "d": 5, "e": 7, "f": 11, "g": 13, "h": 17, "i": 19, "j": 23, "k": 29,
                     "l": 31, "m": 37, "n": 41, "o": 47, "p": 53, "q": 59, "s": 61, "t": 67, "u": 71, "v": 73, "w": 79,
                     "x": 83, "y": 89, "z": 97, "1": 101, "2": 103, "3": 107, "4": 111, "5": 113, "6": 117, "7": 119, "8": 123, "9": 127, "0": 131}

    def __init__(self):
        for i in range(26):
            self.alphabetWages[i] = i

    def convert_to_code(self, word):
        result = 0
        string_split = list(word)
        string_split = lower(string_split)
        for i in range(0, len(word)):
            for letter in self.alphabetWages:
                if letter == string_split[i]:
                    result += (i+1) * self.alphabetWages[letter]
        return result

    def convert_file(self, path):
        try:

            codedWords = []
            file = open(path, 'r')
            tab = []
            for line in file:
                string = ' '.join(line.split())
                tab.append(string)
            file.close()
            file = open(path, "w")
            for line in tab:
                file.write(line)
            file.close()
            file = open(path, 'r').read()
            words = file.split(" ")
            return words
        except IOError:
            print("bledna sciezka do pliku")




str = StrCode()
print(str.convert_file("lorem.txt"))


import csv
from numpy.core.defchararray import lower


class CsvCoder:
    path = ""

    csvData = []

    def __init__(self, path):
        self.path = path
        self.csvData = self.load_csv()
        self.csvDataWithoutResult = self.delete_result()
        self.result = self.split_result()

    def load_csv(self):
        try:
            result =[]
            lines = csv.reader(open(self.path))
            dataset = list(lines)
            for i in range(len(dataset) - 1):
                temp = []
                for j in dataset[i]:
                    temp.append(int(j))
                result.append(temp)
            return result
        except IOError:
            print("no such file!")

    def delete_result(self):
        tab = self.csvData
        result = []
        for i in range(len(tab)):
            result_row = []
            for j in range(len(tab[1]) - 1):
                result_row.append(tab[i][j])
            result.append(result_row)
        return result

    def split_result(self):
        tab = self.csvData
        result = []
        for i in tab:
            result.append(i[len(i)-1])
        return result

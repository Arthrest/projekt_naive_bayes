import statistics_calculation


class NaiveBayes():

    def __init__(self,debug_log):
        self.debug_log = debug_log
        self.collected_statistics = {}
        self.class_lengths = {}
        self.accuracy = 0

    # ============================================ PRIVATE ========================================================
    # ============= debug_logger ===================================================
    # @param text    - string to be printed on screen
    def debug_logger(self, text):
        if self.debug_log == True:
            print("LOG: " + text)

    # ============= separate_data_on_classes =======================================
    # @param data_to_separate    - array of data to separate, last element is treated
    #                              as class name (qualifier), example data format:
    #                              [attribute1, attribute2, class_name]
    #                                               ||
    #                              [[0.2, 0.5, 0], [1, 1.2, 1], [0.8, 1.8, 0]] -> class_names: 0 and 1
    #
    # @return                    - data separated on classes in form of dictionary
    def separate_data_on_classes(self, data_to_separate):
        separated_data = {}
        for x in range(len(data_to_separate)):
            # take each line in set
            line = data_to_separate[x]
            # if last value (class name) doesn't exist yet, create new list for this one
            # example of line: [attribute1, attribute2, attribute3, class_name]
            if (line[-1] not in separated_data):
                separated_data[line[-1]] = []
            # if it does exist, just add it to existing list
            separated_data[line[-1]].append(line)
        return separated_data

    # ============= calculate_events_statistics =======================================
    # @param dataset    - dictionary of data separated on classes
    # @return           - dictionary of statistics (mean and standard deviations) for each separated class
    def calculate_events_statistics(self, dataset):
        statistics = {}
        # get data sets for each class
        for class_nr in dataset:
            temp_store = []
            # mean value calculated for each attribute in class
            mean = []
            # standard deviation calculated for each attribute in class
            stddev = []
            # separate attributes using zip method (each attribute must has got mean and stddev - example - mean and stddev for temperature attribute)
            for event in zip(*dataset[class_nr]):
                temp_store.append(event)
            # len - 1 because we don't want to take last field (class number)
            # calculate mean and stddev for each one attribute for each one class
            for x in range(len(temp_store) - 1):
                mean.append(statistics_calculation.calculate_mean(temp_store[x]))
                stddev.append(statistics_calculation.calculate_standard_deviation(temp_store[x]))
            statistics[class_nr] = [mean, stddev]
        return statistics

    # ============= calculate_events_probabilities =======================================
    # @param event      - array describing some event (attributes and class name)
    # @param statistics - dictionary of statistics for each separated class
    # @return           - dictionary of probabilities for each separated class
    def calculate_events_probabilities(self, event, statistics):
        probability = {}
        # calculate probability of belonging event to each class
        for class_nr in statistics:
            # default value for probability
            probability[class_nr] = 1
            attribute = statistics[class_nr]
            # calculate probability density of each one attribute and multiply them
            for x in range(len(event)):
                mean = attribute[0]
                stdev = attribute[1]
                probability[class_nr] = probability[class_nr] * statistics_calculation.calculate_probability_density(event[x], mean[x], stdev[x])
            probability[class_nr] *= self.class_lengths[class_nr]
        sum = 0


        for x in probability:
            sum = sum + probability[x]

        for y in probability:
            probability[y] = probability[y]/sum

        return probability

    # ============================================ PUBLIC ========================================================

    # ============= train_algorithm =======================================
    # @param test_dataset    - array of data to get statistics and train algorithm
    def train_algorithm(self, test_dataset):
        self.debug_logger("Starting training algorithm procedure ...")

        # separate given dataset depending on classes names (last values in each line)
        separated_dataset = self.separate_data_on_classes(test_dataset)

        for class_nr in separated_dataset:
            self.class_lengths[class_nr] = len(separated_dataset[class_nr])

        self.debug_logger("Step 1. Separated data: " + str(separated_dataset))

        self.collected_statistics = self.calculate_events_statistics(separated_dataset)
        self.debug_logger("Step 2. Collected statistics from training set: " + str(self.collected_statistics))

        self.debug_logger("Training algorithm finished.")

    # ============= predict_result =======================================
    # @param event      - array describing some event attributes (without result - class name)
    def predict_result(self, event):
        self.debug_logger("Starting predicting result procedure ...")

        probability = self.calculate_events_probabilities(event,self.collected_statistics)
        self.debug_logger("Step 1. Calculated probabilities of all events: " + str(probability))

        max_probability = 0
        predicted_result = None
        for class_number in probability:
            if (probability[class_number] > max_probability):
                max_probability = probability[class_number]
                predicted_result = class_number

        self.debug_logger("Step 2. Predicted event and its probability" + str((predicted_result, max_probability)))
        return (predicted_result, max_probability)

    def predict_results(self, events):
        results_array = []
        counter = 0
        for x in range(len(events)):
            event = events[x]
            answer = event[-1]
            attributes = event[:(len(event) - 1)]
            result = self.predict_result(attributes)
            results_array.append(result)
            if result[0] == answer:
                counter += 1
        self.accuracy = (counter / len(events)) * 100
        return results_array

    def get_accuracy(self):
        return self.accuracy
import test
from myhdl import always, delay, now, block, instance


@block
def naiveBayesWrapper():

    @always(delay(10))
    def say_hello():
        print("hello world!")

    return say_hello


inst = naiveBayesWrapper()
inst.run_sim(10)
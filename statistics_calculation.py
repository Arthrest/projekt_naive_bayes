import math


# ============= calculate_mean =======================================
# @param data    - array of numbers from which mean should be calculated
# @return        - mean value of the given data set
def calculate_mean(data):
    # calculate how many numbers there are in data set
    data_len = len(data)
    sum = 0
    # sum all of the numbers in data set
    for x in range(data_len):
        sum = sum + data[x]
    # divide sum by amount of numbers in set to get mean value
    return sum/data_len


# ============ calculate_standard_deviation ==========================
# @param data   - array of numbers from which standard deviation should be calculated
# @return       - standard deviation of the given data set
def calculate_standard_deviation(data):
    # get mean value of data set
    data_mean = calculate_mean(data)
    # calculate how many numbers there are in data set
    data_len = len(data)
    result = 0
    for x in range(data_len):
        result = result + pow(data[x]-data_mean, 2)
    result = math.sqrt(result/data_len)
    return result


# =========== calculate_probability_density ===========================
# Implements gaussian formula to calculate probability density function
# @param value   - value for which probability density should be calculated
# @param mean    - mean value of data set to which given value could belong
# @param stddev  - standard deviation of data set to which given value could belong
# @return        - probability density of situation in which given value could belong to data set
def calculate_probability_density(value, mean, stddev):
    # exponenta factor
    exp = math.exp((-1)*pow(value-mean, 2)/(2*pow(stddev, 2)))
    result = exp/(stddev*math.sqrt(2*math.pi))
    return result

import math
import sklearn
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB

from csv_to_code import CsvCoder as coder
import naive_bayes

# stworzenie instancji algorytmu (wpisując true debug log jest włączony, false - wyłączony)
bayes = naive_bayes.NaiveBayes(False)
# trenowanie algorytmu stworzoną tablicą
coderCsv = coder("forecast.csv")
bayes.train_algorithm(coderCsv.csvData)
# ============================== sprawdzenie poprawności pracy algorytmu ============================================================
# oblicz przewidywany rezultat podanego zdarzenia i jakie jest tego prawdopodobieństwo (dla przykładowych danych z tego samego pliku)
print("Wynik naszego algorytmu dla zdarzenia 1: " + str(bayes.predict_result(coderCsv.csvDataWithoutResult[0])))
print("Wynik naszego algorytmu dla zdarzenia 2: " + str(bayes.predict_result(coderCsv.csvDataWithoutResult[1])))
print("Wynik naszego algorytmu dla zdarzenia 3: " + str(bayes.predict_result(coderCsv.csvDataWithoutResult[2])))
print("Wynik naszego algorytmu dla zdarzenia 4: " + str(bayes.predict_result(coderCsv.csvDataWithoutResult[3])))
print("Wynik naszego algorytmu dla zdarzenia 5: " + str(bayes.predict_result(coderCsv.csvDataWithoutResult[4])))
#bayes.predict_results([coderCsv.csvDataWithoutResult[0], coderCsv.csvDataWithoutResult[1], coderCsv.csvDataWithoutResult[2], coderCsv.csvDataWithoutResult[3], coderCsv.csvDataWithoutResult[4]])

#tworzenie i trenowanie wbudowanego algorytmu dla porównania wyników
gnb = GaussianNB()
gnb.fit(coderCsv.csvDataWithoutResult, coderCsv.result)
# tworzenie tych samych zdarzeń co poprzednio
events  = [coderCsv.csvDataWithoutResult[0],coderCsv.csvDataWithoutResult[1],coderCsv.csvDataWithoutResult[2],coderCsv.csvDataWithoutResult[3],coderCsv.csvDataWithoutResult[4]]
events_pred = gnb.predict(events)
events_prob = gnb.predict_proba(events)

result1 = events_prob[0]
result2 = events_prob[1]
result3 = events_prob[2]
result4 = events_prob[3]
result5 = events_prob[4]

print("Wynik wbudowanego algorytmu dla zdarzenia 1: " + str(events_pred[0]) + ", " + str(result1[events_pred[0]]))
print("Wynik wbudowanego algorytmu dla zdarzenia 2: " + str(events_pred[1]) + ", " + str(result2[events_pred[1]]))
print("Wynik wbudowanego algorytmu dla zdarzenia 3: " + str(events_pred[2]) + ", " + str(result3[events_pred[2]]))
print("Wynik wbudowanego algorytmu dla zdarzenia 4: " + str(events_pred[3]) + ", " + str(result4[events_pred[3]]))
print("Wynik wbudowanego algorytmu dla zdarzenia 5: " + str(events_pred[4]) + ", " + str(result5[events_pred[4]]))

# ================================== obliczenie dokładności algorytmu ======================================================
# deklaracja pomocniczych zmiennych ułatwiających zrozumienie testu
# pogoda
sunny = 0
overcast = 1
rainy = 2
# wilgotność
high = 0
normal = 1
# wiatr
weak = 0
strong = 1
# czy da się bawić na zewnątrz?
no = 0
yes = 1

# tworzenie prawdopodobnych zdarzeń dla których algorytm powinien działać dobrze
realistic_events = [[sunny, normal, weak, yes], [rainy, normal, weak, yes], [overcast, normal, weak, yes], [sunny, high, weak, yes], [overcast, high, strong, no]]
bayes.predict_results(realistic_events)
print("Dokładnosc algorytmu dla prawdopodobnych zdarzeń: " + str(bayes.get_accuracy()) + "%")

# tworzenie zdarzeń nielogicznych i niepasujących do statystyki z którymi algorytm może mieć problemy
unrealistic_events = [[sunny, high, strong, yes], [rainy, high, strong, yes], [sunny, normal, weak, no], [sunny, high, weak, yes], [overcast, high, strong, no]]
bayes.predict_results(unrealistic_events)
print("Dokładnosc algorytmu dla nieprawdopodobnych zdarzeń: " + str(bayes.get_accuracy()) + "%")


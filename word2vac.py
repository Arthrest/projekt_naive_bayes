from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot

class VectorConverter:
	path = ""
	sentences = []
	model = []

	def __init__(self, path):
		self.path = path
		self.read_file()
		self.convert_file()

	def read_file(self):
		try:
			codedWords = []
			file = open(self.path, 'r')
			tab = []
			for line in file:
				string = ' '.join(line.split())
				tab.append(string)
			for string in tab:
				words = string.split(" ")
				self.sentences.append(words)
			file.close()
		except IOError:
			print("bledna sciezka do pliku")

	def convert_file(self):
		self.model = Word2Vec(self.sentences, min_count=1, alpha=1, size=5)


converted = VectorConverter("Adolf Hitler.txt")
print(converted.model)

print(converted.model.predict_output_word(["Adolf"]))

# X = converted.model[converted.model.wv.vocab]
# print(X)
# pca = PCA(n_components=2)
# result = pca.fit_transform(X)
# # create a scatter plot of the projection
# pyplot.scatter(result[:, 0], result[:, 1])
# words = list(converted.model.wv.vocab)
# for i, word in enumerate(words):
# 	pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
# pyplot.show()


